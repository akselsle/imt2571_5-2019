<?php
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */

class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */
    public function getActorStatistics()
    {
        $result = array();
        $actorResult = array();
        $rolesForActors = array();
        //$rolesForActorsObj = array();


        $actors = $this->xpath->query('/Disney/Actors/Actor/@id');

        //Finner @id (navn) for hver actor i actors.
          foreach ($actors as $actor){
            $actorResult[] = $actor->value;
            
             //Får tak i navn med space != @id:
             $nameWithSpacesObj = $this->xpath->query("/Disney/Actors/Actor[@id='$actor->value']/Name");
             $actorNameWithSpaces = $nameWithSpacesObj->item(0)->nodeValue;
             $result[$actorNameWithSpaces] = array();

          }
          //codecept_debug($actorResult);
          //Finner alle roller hver actorName(@id) har.  Kan bruke dette til ->parent->parent.

          foreach($actorResult as $actorName){
             $rolesForActorsObj = $this->xpath->query("/Disney/Subsidiaries/Subsidiary/Movie/Cast/Role[@actor ='$actorName']");
              foreach($rolesForActorsObj as $actorRole){
                $actorInfo = "As " . $actorRole->attributes[0]->value . " in ";    //lager en string og leggger inn '@alias'
                $actorMovie = $actorRole->parentNode->parentNode->childNodes;  //Går opp til 'Movie'
                foreach($actorMovie as $movie){

                  //Fjerner noder jeg ikke vil ha:
                  if($movie->nodeName != '#text' && $movie->nodeName != 'Cast'
                    &&  $movie->nodeName != 'ScreenTime'){

                      //formatterer
                    if($movie->nodeName == 'Name')
                        $actorInfo .= $movie->nodeValue;
                    else
                        $actorInfo .= ' (' . $movie->nodeValue . ')';
                  }
                }

                //Får tak i navn med space != @id:
                $nameWithSpacesObj = $this->xpath->query("/Disney/Actors/Actor[@id='$actorName']/Name");
                $actorNameWithSpaces = $nameWithSpacesObj->item(0)->nodeValue;

                  //appender/pusher på ny info:
                  array_push($result[$actorNameWithSpaces],$actorInfo);
              
            }
          }        
        //To do:
        // Implement functionality as specified

        return $result;
    }        //codecept_debug($list);

    /**
      * Removes Actor elements from the $doc object for Actors that have not
      * played in any of the movies in $doc - i.e., their id's do not appear
      * in any of the Movie/Cast/Role/@actor attributes in $doc.
      */
    public function removeUnreferencedActors()
    {
      // $list = $this->getActorStatistics();
      // foreach($list as $actor){
      //    if(count($actor) == 0)
      //      unset($list[$actor]);
        
      //   codecept_debug(count($actor));
      // }
    }

    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null)
    {
        //To do:
        // Implement functionality as specified
        $role = $this->doc->createElement('Role');
        $roleElement = $this->xpath->query('Role')->item(0);
        $roleElement->appendChild($role);

        //TODO: Legge ved de ulike attributer: 
        $roleElement->setAttribute('');

    }
}

?>
